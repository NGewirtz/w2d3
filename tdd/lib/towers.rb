class TowersOfHanoi
  attr_accessor :board

  def initialize
    @board = [[3, 2, 1], [], []]
  end

  def get_move
    gets.chomp.split(',').map(&:to_i)
  end

  def move(position)
    raise "No Disk" if @board[position[0]].empty?
    raise "Invalid Move" unless valid_move?(position)
    @board[position[1]] << @board[position[0]].pop
  end

  def won?
    @board[1] == [3, 2, 1] || @board[2] == [3, 2, 1]
  end

  private

  def valid_move?(pos)
    return false if pos[1] > 2
    return true if @board[pos[1]].empty?
    return false if @board[pos[0]].last > @board[pos[1]].last
    true
  end

end



#move(get_move)
