def my_uniq(array)
  hash = Hash.new
  array.each { |el| hash[el] = true }
  hash.keys
end

def two_sum(array)
  i = 0
  result = []
  while i < array.length
    j = i + 1
    while j < array.length
      result << [i, j] if array[i] + array[j] == 0
      j += 1
    end
    i += 1
  end

  result
end

def my_transpose(array)
  arr = []
  i = 0
  while i < array.length
    col = []
    array.each do |el|
      col << el[i]
    end
    arr << col
    i += 1
  end
  arr
end

def stock_picker(arr)
  current_idx = nil
  current_value = 0
  i = 0
  while i < arr.length
    j = i + 1
    while j < arr.length
      if arr[j] - arr[i] > current_value
        current_idx = [i , j]
        current_value = arr[j] - arr[i]
      end
      j += 1
    end
    i += 1
  end
  current_idx
end
