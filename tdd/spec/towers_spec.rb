require "towers"

describe TowersOfHanoi do

  subject(:game) { TowersOfHanoi.new }

  describe "#initialize" do

    it "sets up the board correctly" do
      expect(game.board).to eq([[3, 2, 1], [], []])
    end
  end

  let(:position) { [2, 0] }

  describe "#move" do
    it "raises error if there's no disk on the tower" do
      expect { game.move(position) }.to raise_error("No Disk")
    end

    it "allows user to make valid moves" do
      game.move([0,1])
      expect(game.board).to eq([[3,2], [1], []])
    end

    it "raises error if the destination disk is greater than selected disk" do
      game.move([0,1])
      expect { game.move([0,1]) }.to raise_error("Invalid Move")
    end

    it "raises error if user inputs an invalid integer as destination" do

      expect { game.move([0,7]) }.to raise_error("Invalid Move")
    end

  end

  describe "#won?" do
    it "returns false if the game is not won" do
      expect(game.won?).to be false
    end

    it "returns true if the game is won" do
      game.board = [[], [], [3, 2, 1]]
      expect(game.won?).to be true
      game.board = [[],[3, 2, 1],[]]
      expect(game.won?).to be true
    end
  end
end
