require 'tdd'

describe "#my_uniq" do

  let(:array) { [1, 2, 1, 3, 3] }
  let(:array2) { [3, 3, 5, 7, 7, 9, 8, 9] }

  it "returns a new array of unique elements" do
    expect(my_uniq(array)).to eq([1, 2, 3])
    expect(my_uniq(array2)).to eq([3, 5, 7, 9, 8])
  end

  it "doesn't call the built in #uniq method" do
    expect(array).to_not receive(:uniq)
    my_uniq(array)
  end
end

describe "#two_sum" do

  it "returns pairs in sorted order" do
    expect(two_sum([-1, 0, 2, -2, 1])).to eq([[0, 4], [2, 3]])
    expect(two_sum([1, 2, 3, -3, 7, 8 ,9])).to eq([[2, 3]])
  end

  it "returns an empty array if there are no pairs" do
    expect(two_sum([1, 2, 3, 4])).to eq([])
  end
end

describe "#my_transpose" do

  let(:rows) { [[0, 1, 2], [3, 4, 5], [6, 7, 8]] }
  let(:cols) { [[0, 3, 6], [1, 4, 7], [2, 5, 8]] }

  it "converts rows to columns" do
    expect(my_transpose(rows)).to eq(cols)
    expect(my_transpose(cols)).to eq(rows)
  end
end


describe "#stock_picker" do

  let(:prices) { [10, 1, 5, 8, 0] }
  let(:prices2) { [10, 5, 4, 3, 2 ] }

  it "returns an array of correct indices" do
    expect(stock_picker(prices)).to eq([1, 3])
  end

  it "return nil if there are no profitable pairs" do
    expect(stock_picker(prices2)).to be nil
  end

end
