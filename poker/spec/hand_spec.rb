require 'hand'
require 'card'

describe Hand do
  subject(:hand) { Hand.new }
  let(:exhand) { Hand.new }

  describe '#initialize' do

    it 'initializes with an empty hand' do
      expect(hand.cards).to eq([])
    end

  end

  describe '#value' do
    it 'finds one-pair' do
      exhand.cards = [Card.new('2', :D), Card.new('2', :C), Card.new('4', :D), Card.new('5', :D), Card.new('6', :D)]
      expect(exhand.value).to eq('one-pair')
    end

    it 'finds two-pair' do
      exhand.cards = [Card.new('2', :D), Card.new('2', :C), Card.new('4', :D), Card.new('4', :D), Card.new('6', :D)]
      expect(exhand.value).to eq('two-pair')
    end

    it 'finds three-of-a-kind' do
      exhand.cards = [Card.new('2', :D), Card.new('2', :C), Card.new('2', :H), Card.new('4', :D), Card.new('6', :D)]
      expect(exhand.value).to eq('three-of-a-kind')
    end

    describe '#straight?' do
      context 'when cards are non-face cards' do
        it 'finds straight' do
          exhand.cards = [Card.new('2', :D), Card.new('3', :C), Card.new('4', :D), Card.new('5', :D), Card.new('6', :D)]
          expect(exhand.value).to eq('straight')
          exhand.cards = [Card.new('8', :D), Card.new('10', :D), Card.new('9', :C), Card.new('Q', :D), Card.new('J', :D)]
          expect(exhand.value).to eq('straight')
        end
      end


      context 'when ace is high' do
        it 'finds straight' do
          exhand.cards = [Card.new('J', :D), Card.new('10', :D), Card.new('K', :C), Card.new('Q', :D), Card.new('A', :D)]
          expect(exhand.value).to eq('straight')
        end
      end

      context 'when ace is low' do
        it 'finds straight' do
          exhand.cards = [Card.new('4', :D), Card.new('5', :D), Card.new('2', :C), Card.new('3', :D), Card.new('A', :D)]
          expect(exhand.value).to eq('straight')
        end
      end
    end


    it 'finds flush' do
      exhand.cards = [Card.new('2', :D), Card.new('4', :D), Card.new('6', :D), Card.new('10', :D), Card.new('J', :D)]
      expect(exhand.value).to eq('flush')
    end

    it 'finds full-house' do
      exhand.cards = [Card.new('2', :D), Card.new('2', :H), Card.new('6', :D), Card.new('6', :C), Card.new('6', :S)]
      expect(exhand.value).to eq('full-house')
    end

    it 'finds four-of-a-kind' do
      exhand.cards = [Card.new('2', :D), Card.new('2', :C), Card.new('2', :H), Card.new('2', :S), Card.new('6', :S)]
      expect(exhand.value).to eq('four-of-a-kind')
    end

    it 'finds a royal flush' do
      exhand.cards = [Card.new('10', :D), Card.new('J', :D), Card.new('Q', :D), Card.new('K', :D), Card.new('A', :D)]
      expect(exhand.value).to eq('royal-flush')
    end
  end
end
