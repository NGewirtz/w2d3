require 'player'

describe Player do
  subject(:player) { Player.new }
  let(:hand) { double("hand", cards: [0, 1, 2, 3, 4]) }
  describe '#initialize' do

    it 'gets a hand' do
      expect(player.hand).to be_a(Hand)
    end

    it 'gets a pot' do
      expect(player.pot).to eq(1000)
    end
  end

  describe '#discard' do
    let(:user_input) { "1,3,4"}
    it 'takes card position and return non discarded cards' do
      player.hand.cards = [0, 1, 2, 3, 4]
      expect(player.discard(user_input)).to eq([1,4])
    end

    it 'discards the right cards' do
      player.hand.cards = [0, 1, 2, 3, 4]
      player.discard(user_input)
      expect(player.hand.cards).to eq([1,4])
    end

  end

end
