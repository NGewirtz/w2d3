require 'game'
require 'byebug'
describe Game do

    subject(:game) { Game.new(3) }
    # let(:player) { double('player', hand: hand ) }
    # let(:deck) { double('deck', deal: [1,2,3,4,5] ) }
    # let(:hand) {double('hand', "cards=" => [1,2,3,4,5])}

    describe '#initialize' do

      it 'creates an array with the number of specified players' do
        expect(game.players).to be_a(Array)
        expect(game.players.length).to eq(3)
        expect(game.players[0]).to be_a(Player)
      end

      it 'initializes with a deck' do
        expect(game.deck).to be_a(Deck)
      end
    end


    describe '#deal' do
      it 'deals each player 5 cards' do
        # expect(player.hand).to receive("cards=")
        expect(game.deck).to receive(:deal_cards).exactly(3).times
        game.deal
        debugger
        expect(game.players.all? do |player|
          player.hand.cards.length == 5
        end).to be true
      end
    end
end
