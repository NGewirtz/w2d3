require 'deck'

describe Deck do
  subject(:deck) { Deck.new }

  let(:deck2) { Deck.new }

  let(:shuffled_deck) { Deck.shuffled_deck }

  describe '#initialize' do

    it 'initializes with 52 cards' do
      expect(deck.cards.count).to eq(52)
    end
  end

  describe '#shuffle!' do

    it 'shuffles the deck' do
      deck2.shuffle!
      expect(deck2.cards).not_to eq(deck.cards)
    end
  end

  describe 'Deck::shuffled_deck' do

    it "initializes correctly with a shuffled deck" do
      expect(deck.cards).not_to eq(shuffled_deck.cards)
    end

  end

  describe '#deal' do
    it 'removes one card from the deck if no argument is given' do
      deck.deal
      expect(deck.cards.length).to eq(51)
    end

    it 'removes several cards from the deck' do
      deck.deal(5)
      expect(deck.cards.length).to eq(47)
    end

    it 'returns the proper number of cards' do
      expect(deck.deal(5).length).to eq(5)
    end

  end

  describe '#reshuffle' do
    it 'repopulates a new shuffled deck' do
      deck.deal(15)
      deck.reshuffle!
      expect(deck.cards.length).to eq(52)
    end
  end
end
