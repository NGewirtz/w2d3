require 'card'

describe Card do
  subject(:card) { Card.new("7", :D) }

  describe "#initialize" do

    it 'takes in a rank' do
      expect(card.rank).to eq("7")
    end

    it 'takes in a suit' do
      expect(card.suit).to eq(:D)
    end
  end
end
