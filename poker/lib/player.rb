class Player

  attr_accessor :pot, :hand

  def initialize(pot = 1000)
    @hand = Hand.new
    @pot = pot
  end

  def discard(positions)
    positions.split(",").sort.reverse.map { |idx| hand.cards.delete_at(idx.to_i - 1) }
    @hand.cards
  end

  def get_move
    move = Integer(gets.chomp)

    case move
    when 1
      :fold
    when 2
      :call
    when 3
      :raise
    end
  end

end
