require_relative 'card'

class Deck
  SUITS = [:D, :C, :H, :S]
  RANKS = %w(2 3 4 5 6 7 8 9 10 J Q K A)

  attr_reader :cards

  def initialize
    @cards = []
    make_deck
  end

  def shuffle!
    @cards.shuffle!
    self
  end

  def self.shuffled_deck
    Deck.new.shuffle!
  end

  def deal_cards(num = 1)
    arr = []
    num.times do
      arr << @cards.pop
    end
    arr
  end

  def reshuffle!
    @cards = []
    make_deck.shuffle!
  end

  private

  def make_deck
    SUITS.each do |suit|
      RANKS.each do |rank|
        @cards << Card.new(rank, suit)
      end
    end
    @cards
  end

end
