class Hand
  attr_accessor :cards

  def initialize
    @cards = []
  end

  def value
    case
    when royal_flush?
      "royal-flush"
    when straight_flush?
      "straight_flush"
    when flush?
      "flush"
    when straight?
      "straight"
    when full_house?
      "full-house"
    when one_pair?
      "one-pair"
    when two_pair?
      "two-pair"
    when three_of_a_kind?
      "three-of-a-kind"
    when four_of_a_kind?
      "four-of-a-kind"
    end
  end

  private
  def one_pair?
    find_pairs.length == 2
  end

  def two_pair?
    find_pairs.length == 4
  end

  def three_of_a_kind?
    find_ranks.any? { |rank| find_ranks.count(rank) == 3 }
  end

  def straight?
    straight_possibilities.include?(find_ranks.sort)
  end

  def straight_possibilities
    %w(A 2 3 4 5 6 7 8 9 10 J Q K A).each_cons(5).map(&:sort)
  end

  def find_ranks
    @cards.map(&:rank)
  end

  def find_pairs
    find_ranks.select { |rank| find_ranks.count(rank) == 2 }
  end

  def flush?
    @cards.map(&:suit).uniq.count == 1
  end

  def full_house?
    three_of_a_kind? && one_pair?
  end

  def straight_flush?
    straight? && flush?
  end

  def four_of_a_kind?
    find_ranks.any? { |rank| find_ranks.count(rank) == 4 }
  end

  def royal_flush?
    find_ranks.sort == %w(10 J K Q A).sort && flush?
  end

end
