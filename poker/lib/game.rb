require_relative 'player'
require_relative 'deck'
require_relative 'hand'
require 'byebug'

class Game
  attr_accessor :players, :deck

  def initialize(players = 4)
    @players = []
    players.times { @players << Player.new }
    @deck = Deck.shuffled_deck
  end

  def deal
    debugger
    @players.each do |player|
      player.hand.cards = @deck.deal_cards(5)
    end
  end

end
